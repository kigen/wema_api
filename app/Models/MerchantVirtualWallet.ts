import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Merchant from './Merchant'

export default class MerchantVirtualWallet extends BaseModel {
  @column({ isPrimary: true })
  public merchantWalletAccountId: number

  @column()
  public virtualAccountNumber: string

  @column()
  public merchantId: number

  @column()
  public virtualAccountProvider: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=>Merchant)
  public merchant: BelongsTo<typeof Merchant>
}
