import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Merchant from './Merchant'

export default class Payment extends BaseModel {
  @column({ isPrimary: true })
  public paymentId: number

  @column()
  public paymentReference: string

  @column()
  public amount: number

  @column()
  public merchantId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=>Merchant)
  public merchant: BelongsTo<typeof Merchant>
}
