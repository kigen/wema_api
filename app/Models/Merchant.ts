import { DateTime } from 'luxon'
import { BaseModel, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Payment from './Payment'
import MerchantVirtualWallet from './MerchantVirtualWallet'

export default class Merchant extends BaseModel {
  @column({ isPrimary: true })
  public merchantId: number

  @column()
  public name: string

  @column()
  public bankAccountNumber: string

  public bankCode: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(()=>Payment)
  public payments: HasMany<typeof Payment>

  @hasMany(()=>MerchantVirtualWallet)
  public virtualAccounts: HasMany<typeof MerchantVirtualWallet>
}
