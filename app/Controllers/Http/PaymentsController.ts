import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import MerchantVirtualWallet from 'App/Models/MerchantVirtualWallet'
import Payment from "App/Models/Payment"
import Merchant from "App/Models/Merchant"

export default class PaymentsController {

   public async notification({ request, response }: HttpContextContract) {
      var body = request.body()
      var merchant = MerchantVirtualWallet.findByOrFail("virtual_account_number", body["craccount"])
      var payment = Payment.create(
         {
            "paymentReference": body["paymentreference"],
            "amount": body["amount"],
            "merchantId": (await merchant).merchantId
         }
      )

      if ((await payment).paymentId) {
         response.status(200)
         return {
            "transactionreference": (await payment).paymentId, //Unique reference from the Vendor 
            "status": "00",
            "status_desc": "Success"
         }
      }
      else {
         response.status(400)
         return {
            "transactionreference": "N/A", //Unique reference from the Vendor 
            "status": "0",
            "status_desc": "Transaction Failed"
         }
      }

   }

   public async validation({ request, response }: HttpContextContract) {
      var body = request.body();
      try {
         response.status(200)
         var virtualAccount = await MerchantVirtualWallet.findByOrFail("virtual_account_number", body["accountnumber"])
         var merchant = await Merchant.find(virtualAccount.merchantId) 
   
         return {
            "accountname": merchant?.name,
            "status": "00",
            "status_desc": "success"
         }
      } catch (e) {
         console.log(e)
         return {
            "accountname": "Unknown",
            "status": "07",
            "status_desc": "invalid account number"
         }
      }

   }
}
