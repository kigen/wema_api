import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Merchant from 'App/Models/Merchant'
import MerchantVirtualWallet from 'App/Models/MerchantVirtualWallet'

export default class MerchantsController {

    public async add({ request, response }: HttpContextContract) {
        var body = request.body()
        var merchant = Merchant.create(body)
        response.status(201)
        return merchant
    }

    public async list({ }: HttpContextContract) {
        return Merchant.all()
    }

    public async createVirtualAccount({ params }: HttpContextContract) {

        var merchant = await Merchant.find(params.merchantId)

        var virtualAccount = MerchantVirtualWallet.create(
            {
                "virtualAccountNumber": "9420" + Math.round(Math.random() * 10000),
                "virtualAccountProvider": "Wema Bank",
                "merchantId": merchant?.merchantId
            }
        )

        return virtualAccount
    }

}
