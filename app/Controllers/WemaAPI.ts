import axios from 'axios'
import env from 'env'
import Encryption from '@ioc:Adonis/Core/Encryption'

export default class WemaAPI {
    baseUrl: string
    constructor(url: string) {
        this.baseUrl = url
    }

    async authenticate() {
        axios.post(
            this.baseUrl + "/api/Authentication/authenticate",
            {
                username: env.WEMA_DISBURSE_USERNAME,
                password: env.WEMA_DISBURSE_PASSWORD
            }
        ).then(response => {
            return {
                success: true,
                data: response.data
            }
        }).catch(error => {
            console.log(error)
            return {
                success: false,
                data: error
            }
        })
    }

    async refreshToken(refreshToken) {
        axios.post(
            this.baseUrl + "/api/Authentication/RefreshToken",
            {
                refreshToken: refreshToken
            }
        ).then(response => {
            return {
                success: true,
                data: response.data
            }
        }).catch(error => {
            console.log(error)
            return {
                success: false,
                data: error
            }
        })
    }

    async disbursePayment() {

    }

    async nameInquiry(accountNumber: string, bankCode: string){
        
        var requestData = { 
            "myDestinationBankCode": accountNumber, 
            "myDestinationAccountNumber": bankCode 
        } 
        var encryptedData = this.encrypt(requestData)
        axios.post(
            this.baseUrl + "/api/Authentication/RefreshToken",
            {
                NameEnquiryRequest: encryptedData
            }
        ).then(response => {
            return {
                success: true,
                data: response.data
            }
        }).catch(error => {
            console.log(error)
            return {
                success: false,
                data: error
            }
        })
    }

    encrypt(data) {
        var payload = JSON.stringify(data)
        return this.algo().encrypt(payload)
    }

    dencrypt(data) {
        var payload = JSON.stringify(data)
        return this.algo().decrypt(payload)
    }

    algo(){
        return Encryption.child({
            secret: env.APP_KEY,
        })
    } 
}