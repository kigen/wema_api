import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class MerchantVirtualWallets extends BaseSchema {
  protected tableName = 'merchant_virtual_wallets'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('merchant_wallet_account_id')
      table.string("virtual_account_number")
      table.string("virtual_account_provider")
      table.integer("merchant_id")
        .unsigned()
        .references("Merchant.merchant_id")
        .onDelete('CASCADE')
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
