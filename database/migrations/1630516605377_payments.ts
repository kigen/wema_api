import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Payments extends BaseSchema {
  protected tableName = 'payments'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('payment_id')
      table.integer("merchant_id")
      .unsigned()
      .references('merchants.merchant_id')
      .onDelete('CASCADE')
      table.double("amount")
      table.string("payment_reference").unique()
      //Omitting other columns for brievity. 
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
