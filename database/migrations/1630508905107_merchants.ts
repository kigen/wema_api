import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Merchants extends BaseSchema {
  protected tableName = 'merchants'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('merchant_id')
      table.string("name", 50)
      table.string("bank_account_number",10) 
      table.string("bank_code")
      //can be another entity (if merchant has more than one account)
      table.double("wallet_balance")
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
